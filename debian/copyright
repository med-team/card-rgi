Format: https://www.debian.org/doc/packaging-manuals/copyright-format/1.0/
Upstream-Name: CARD
Upstream-Contact: card@mcmaster.ca
                  Andrew McArthur <mcarthua@mcmaster.ca>
Source: https://card.mcmaster.ca/download

Files: *
Copyright: 2012-2017 <upstream>
License: non-free
 -> https://card.mcmaster.ca/about

Files: debian/*
Copyright: 2017 Andreas Tille <tille@debian.org>
License: <license>


Date: Thu, 26 Oct 2017 16:24:11 +0000
From: "Wilson, Leigh" <wilsle@mcmaster.ca>
To: "McArthur, Andrew" <mcarthua@mcmaster.ca>, Andreas Tille <andreas@an3as.eu>
CC: "Raphenya, Amogelang" <raphenar@mcmaster.ca>, "milo, dsk" <milodsk@mcmaster.ca>
Subject: RE: Please consider a free license for CARD RGI

Dear Andreas-

Your recent follow-up on this matter was forwarded to me.  I apologize
if we were not clear on this earlier- as Andrew described in the email
below we must continue to enforce licenses for commercial users in order
to maintain the quality of CARD, I believe this makes it difficult to
support Debian.  I hope you understand our position,

Sincerely,

Leigh

From: McArthur, Andrew
Sent: Friday, May 12, 2017 11:44 AM
To: Andreas Tille
Cc: Raphenya, Amogelang; Wilson, Leigh; milo, dsk
Subject: Re: Please consider a free license for CARD RGI

Hello Andreas,

Apologies for my delayed response. As promised, we released RGI 3.2.0
last week and this new version no longer uses MetaGeneMark, but instead
relies on the open source Prodigal tool for gene prediction. Hopefully
that will resolve this first license issue for Debian.

In regards to the license for CARD & RGI itself, unfortunately like many
biological databases CARD finds it hard to fund biocuration using
traditional grant mechanisms and instead has many industrial partners
and licensees. We cannot entertain free or open licenses. While we could
adjust the “as is” restriction to allow path and other modifications for
installation purposes, CARD/RGI would remain in conflict with Debian Med
due to limits on commercial use, so I’m afraid we are unable to support
Debian Med.

We do offer power users support via Conda as our license works within
that model and Debian Med users could secondarily install CARD RGI on
their machines after satisfying license requirements.

Sorry we cannot help more.

Andrew

------
Andrew G. McArthur, Ph.D.
Associate Professor & Cisco Research Chair in Bioinformatics
M.G. DeGroote Institute for Infectious Disease Research,
Department of Biochemistry and Biomedical Sciences,
McMaster University, Hamilton, Ontario L8S 4K1, Canada

office. MDCL 2322 | lab. MDCL 2317
w. mcarthurbioinformatics.ca<http://mcarthurbioinformatics.ca> | e. mcarthua@mcmaster.ca<mailto:mcarthua@mcmaster.ca>
p. (905) 525-9140 ext. 21663 | skype. agmcarthur


[cid:image001.png@01D34E55.53CF2F30]

On May 12, 2017, at 8:43 AM, Andreas Tille <andreas@an3as.eu<mailto:andreas@an3as.eu>> wrote:

Hi,

I'm writing you on behalf of the Debian Med team which is a group inside
Debian with the objective to package free software in life sciences and
medicine for main Debian.  I've got a user request to package CARD RGI[1]

Unfortunately the licensing conditions[2] are to restrictive to be
considered free software.  On one hand it conflicts for instance with
item 5 and 6 of the Debian Free Software Guidelines since it
discrimitates users intending commercial usage.  Even worse the code
is not even distributable inside the Debian non-free repository since

  The Materials not be modified and used "as is"

means we can not even apply some patches that would be really needed to
do some sensible distribution to install the code globally on a machine
for every user (as you even suggest in your installation instruction
_docs/README, paragraph Commands for Running RGI system-wide) since this
would mean changing some pathes inside the code.  For instance when
starting my unfinished packaging attemt I had created some patches[4]
that are permitted by the above sentence.

It would be great if you might consider some well known free license as
for instance is used by the several tools (prodigal, ncbi-blast+ and
python-biopython) you are basing your code upon.  According the
experiences we gathered in the Debian Med team the restrictive license
you are using is not really helping to increase the acceptance of the
code inside the scientific community nor does it encourage others to
provide contributions that would enhance your code.

Kind regards and thanks for considering

      Andreas.


[1] https://card.mcmaster.ca/
[2] https://card.mcmaster.ca/about
[3] https://www.debian.org/social_contract#guidelines
[4] https://anonscm.debian.org/git/debian-med/card-rgi.git/tree/debian/patches

--
http://fam-tille.de


On Apr 29, 2017, at 4:14 PM, McArthur, Andrew <mcarthua@mcmaster.ca<mailto:mcarthua@mcmaster.ca>> wrote:

Hello Andrea,

Much thanks for your email. Your timing is good, we’ve been working on this issue and are about to roll out a new version of RGI that use Prodigal instead.

In addition, we are adjusting the older version downloads to not include the MetaGeneMark binary.

I’ll send you a second email once RGI 3.2.0 based on Prodigal is available.

All the best,
Andrew McArthur

------
Andrew G. McArthur, Ph.D.
Associate Professor & Cisco Research Chair in Bioinformatics
M.G. DeGroote Institute for Infectious Disease Research,
Department of Biochemistry and Biomedical Sciences,
McMaster University, Hamilton, Ontario L8S 4K1, Canada

office. MDCL 2322 | lab. MDCL 2317
w. mcarthurbioinformatics.ca<http://mcarthurbioinformatics.ca/> | e. mcarthua@mcmaster.ca<mailto:mcarthua@mcmaster.ca>
p. (905) 525-9140 ext. 21663 | skype. agmcarthur


<image002.png>

On Apr 25, 2017, at 11:32 AM, Andreas Tille <tille@debian.org<mailto:tille@debian.org>> wrote:

Hello,

I'm writing you on behalf of the Debian Med team which is a group inside
Debian with the objective to package free software in life science and
medicine for official Debian.  I've got a user request to package CARD
RGI and thus I had a look into your download files.

As far as I can see you are including a binary copy of MetaGeneMark
version 3.26 inside the download archive[1].  The LICENSE file included
says:

...
  2. Prohibited Uses.  Licensee may not:

  a) transfer, distribute, lease or sub-license the Product.
...

As far as I understand you've got a licensing key for your use but you
are distributing this together with your software which is prohibited.

I confirm that as a Debian developer I'd really like if software authors
would consider free licenses.  I took the issue above as a reason to
write to GeneMark authors to do change their license[2].  I think it
would be in your interest to join the petition and clarify the license
for your specific case.

In case you actually received a more permissive license from GeneMark
developers please add some additional information to the licensing text.

And BTW, if we are at it:  You might like to consider a free license
for RGI as well. :-)

Kind regards

       Andreas.


[1] https://card.mcmaster.ca/download/1/software-v3.1.1.tar.gz
[2] https://lists.debian.org/debian-med/2017/04/msg00041.html

--
http://fam-tille.de<http://fam-tille.de/>


------
Andrew G. McArthur, Ph.D.
Associate Professor & Cisco Research Chair in Bioinformatics
M.G. DeGroote Institute for Infectious Disease Research,
Department of Biochemistry and Biomedical Sciences,
McMaster University, Hamilton, Ontario L8S 4K1, Canada

office. MDCL 2322 | lab. MDCL 2317
w. mcarthurbioinformatics.ca<http://mcarthurbioinformatics.ca/> | e. mcarthua@mcmaster.ca<mailto:mcarthua@mcmaster.ca>
p. (905) 525-9140 ext. 21663 | skype. agmcarthur


[cid:image001.png@01D34E55.53CF2F30]



